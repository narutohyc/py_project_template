#!/usr/bin/env Python
# -- coding: utf-8 --

"""
@version: v1.0
@author: huangyc
@file: setup.py.py
@Description: https://blog.csdn.net/weixin_43940314/article/details/128349554
@time: 2023/8/2 8:46
"""
import shutil
from pathlib import Path
import os
from setuptools import setup, find_packages
